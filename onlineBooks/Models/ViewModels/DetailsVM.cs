﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace onlineBooks.Models.ViewModels
{
    public class DetailsVM
    {
        public DetailsVM()
        {
            Product = new product();
        }

        public product Product { get; set; }
        public bool ExistsInCart { get; set; }
    }
}
