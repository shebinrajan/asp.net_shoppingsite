﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace onlineBooks_Models.ViewModels
{
    public class HomeVM
    {
        public IEnumerable<product> Products { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}
