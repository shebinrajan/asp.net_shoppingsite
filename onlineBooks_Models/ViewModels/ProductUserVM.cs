﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace onlineBooks_Models.ViewModels
{
    public class ProductUserVM
    {
        public ProductUserVM()
        {
            ProductList = new List<product>();
        }

        public ApplicationUser ApplicationUser { get; set; }
        public IList<product> ProductList { get; set; }
    }
}
