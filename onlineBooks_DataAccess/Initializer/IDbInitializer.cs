﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlineBooks_DataAccess.Initializer
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
