﻿using Microsoft.AspNetCore.Mvc.Rendering;
using onlineBooks.Data;
using onlineBooks_DataAccess.Repository.IRepository;
using onlineBooks_DataAccess.Respository;
using onlineBooks_Models;
using onlineBooks_Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlineBooks_DataAccess.Repository
{
    public class ProductRepository : Repository<product>, IProductRepository
    {
        private readonly ApplicationDbContext _db;
        public ProductRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public IEnumerable<SelectListItem> GetAllDropdownList(string obj)
        {
            if (obj == WC.CategoryName)
            {
                return _db.Category.Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                });
            }
            if (obj == WC.ApplicationTypeName)
            {
                return _db.ApplicationType.Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                });
            }
            return null;
        }

        public void Update(product obj)
        {
            _db.Update(obj);
        }
    }
}
