﻿using Microsoft.AspNetCore.Mvc.Rendering;
using onlineBooks.Data;
using onlineBooks_DataAccess.Repository.IRepository;
using onlineBooks_DataAccess.Respository;
using onlineBooks_Models;
using onlineBooks_Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlineBooks_DataAccess.Repository
{
    public class OrderHeaderRepository : Repository<OrderHeader>, IOrderHeaderRepository
    {
        private readonly ApplicationDbContext _db;
        public OrderHeaderRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
        public void Update(OrderHeader obj)
        {
            _db.Update(obj);
        }
    }
}
