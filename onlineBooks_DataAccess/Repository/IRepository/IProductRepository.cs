﻿using Microsoft.AspNetCore.Mvc.Rendering;
using onlineBooks_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlineBooks_DataAccess.Repository.IRepository
{
    public interface IProductRepository : IRepository<product>
    {
        void Update(product obj);
        IEnumerable<SelectListItem> GetAllDropdownList(string obj);
    }
}
