﻿using Microsoft.AspNetCore.Mvc.Rendering;
using onlineBooks.Data;
using onlineBooks_DataAccess.Repository.IRepository;
using onlineBooks_DataAccess.Respository;
using onlineBooks_Models;
using onlineBooks_Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlineBooks_DataAccess.Repository
{
    public class InquiryDetailRepository : Repository<InquiryDetail>, IInquiryDetailRepository
    {
        private readonly ApplicationDbContext _db;
        public InquiryDetailRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
        public void Update(InquiryDetail obj)
        {
            _db.Update(obj);
        }
    }
}
